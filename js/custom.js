
$(".nav-link").click(function () {
    $(".custom-hamburger").toggleClass("is-active");
});

$(function () {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            ;
            $(".navbar").addClass("nav-shadow");
            $(".navbar-brand").wrapInner("<img src='images/logo-color.png' class='header-logo' id='hideOnTop' alt=''>");
        } else {
            $(".navbar").removeClass("nav-shadow");
            $("#hideOnTop").hide();
            $(".navbar-brand").wrapInner("<img src='images/white-logo.png' class='header-logo' alt=''>");
        }
    });
});
$('.navbar-nav .nav-item').click(function () {
    $('.navbar-nav .nav-item.active').removeClass('active');
    $(this).addClass('active');
});
$(window).scroll(function () {
    var href = $(this).scrollTop();
    $('.link').each(function (event) {
        if (href >= $($(this).attr('href')).offset().top - 1) {
            $('.navbar-nav .nav-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
});

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

AOS.init();


// Carousel de testimonios

var owl = $('#owl-clientes');
owl.owlCarousel({
    loop: true,
    nav: false,
    dots: true,
    autoplay: false,
    margin: 20,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        960: {
            items: 2
        },
        1200: {
            items: 2
        }
    }
});
owl.on('mousewheel', '.owl-stage', function (e) {

    if (e.originalEvent.wheelDelta > 0) {
        owl.trigger('next.owl');
    }
    else {
        owl.trigger('prev.owl');
    }
    e.preventDefault();
});

function init() {
    $('.item').each(function (i) {
        $(this).append('<img src="http://lorempixel.com/400/200/?random=' + i + '" />')
    });

}

init();


// Carousel de servicios

var owl2 = $('#owl-servicios');
owl2.owlCarousel({
    loop: true,
    nav: true,
    dots: false,
    navText: ["<img src='images/arrow-left.svg' class='arrow-carousel arrow-carousel-prev'>", "<img src='images/arrow-right.svg' class='arrow-carousel arrow-carousel-next'>"],
    autoplay: true,
    margin: 20,
    responsive: {
        0: {
            items: 1
        }

    }
});

// Carousel de logos de clientes

var owl3 = $('#owl-logos');
owl3.owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
    autoplay: true,
    margin: 20,
    responsive: {
        0: {
            items: 2,
            margin: 0,
        },
        600: {
            items: 3
        },
        960: {
            items: 4
        },
        1200: {
            items: 6
        }
    }
});

